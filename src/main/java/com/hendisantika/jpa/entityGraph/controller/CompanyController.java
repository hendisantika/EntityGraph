package com.hendisantika.jpa.entityGraph.controller;

import com.hendisantika.jpa.entityGraph.model.Company;
import com.hendisantika.jpa.entityGraph.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : EntityGraph
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/01/18
 * Time: 06.30
 * To change this template use File | Settings | File Templates.
 */


@RestController
@RequestMapping("/company/")
public class CompanyController {

    @Autowired
    @Qualifier(value = "companyServiceEntityGraph")
    private CompanyService companyService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "/withDepartments/{companyId}")
    @ResponseStatus(value = HttpStatus.OK)
    public Company getCompanyWithDepartments(@PathVariable("companyId") Long companyId) {
        return companyService.getCompanyWithDepartments(companyId);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "/withDepartmentsAndEmployees/{companyId}")
    @ResponseStatus(value = HttpStatus.OK)
    public Company getCompanyWithDepartmentsAndEmployees(@PathVariable("companyId") Long companyId) {
        return companyService.getCompanyWithDepartmentsAndEmployees(companyId);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "/withDepartmentsAndEmployeesAndOffices/{companyId}")
    @ResponseStatus(value = HttpStatus.OK)
    public Company getCompanyWithDepartmentsAndEmployeesAndOffices(@PathVariable("companyId") Long companyId) {
        return companyService.getCompanyWithDepartmentsAndEmployeesAndOffices(companyId);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "/withCars/{companyId}")
    @ResponseStatus(value = HttpStatus.OK)
    public Company getCompanyWithCars(@PathVariable("companyId") Long companyId) {
        return companyService.getCompanyWithCars(companyId);
    }

}