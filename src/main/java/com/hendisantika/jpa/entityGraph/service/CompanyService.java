package com.hendisantika.jpa.entityGraph.service;

import com.hendisantika.jpa.entityGraph.model.Company;

/**
 * Created by IntelliJ IDEA.
 * Project : EntityGraph
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/01/18
 * Time: 06.15
 * To change this template use File | Settings | File Templates.
 */

public interface CompanyService {

    Company getCompanyWithDepartments(Long companyId);

    Company getCompanyWithDepartmentsAndEmployees(Long companyId);

    Company getCompanyWithDepartmentsAndEmployeesAndOffices(Long companyId);

    Company getCompanyWithCars(Long companyId);
}
