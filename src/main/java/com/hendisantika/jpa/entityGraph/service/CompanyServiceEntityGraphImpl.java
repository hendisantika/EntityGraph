package com.hendisantika.jpa.entityGraph.service;

import com.hendisantika.jpa.entityGraph.model.Company;
import org.springframework.stereotype.Service;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : EntityGraph
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/01/18
 * Time: 06.20
 * To change this template use File | Settings | File Templates.
 */

@Service(value = "companyServiceEntityGraph")
public class CompanyServiceEntityGraphImpl implements CompanyService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Company getCompanyWithDepartments(Long companyId) {
        EntityGraph graph = entityManager.getEntityGraph("companyWithDepartmentsGraph");
        Map<String, Object> hints = new HashMap<>();
        hints.put("javax.persistence.fetchgraph", graph);

        return entityManager.find(Company.class, companyId, hints);
    }

    @Override
    public Company getCompanyWithDepartmentsAndEmployees(Long companyId) {
        EntityGraph graph = entityManager.getEntityGraph("companyWithDepartmentsAndEmployeesGraph");
        Map<String, Object> hints = new HashMap<>();
        hints.put("javax.persistence.fetchgraph", graph);

        return entityManager.find(Company.class, companyId, hints);
    }

    @Override
    public Company getCompanyWithDepartmentsAndEmployeesAndOffices(Long companyId) {
        EntityGraph graph = entityManager.getEntityGraph("companyWithDepartmentsAndEmployeesAndOfficesGraph");
        Map<String, Object> hints = new HashMap<>();
        hints.put("javax.persistence.fetchgraph", graph);

        return entityManager.find(Company.class, companyId, hints);
    }

    @Override
    public Company getCompanyWithCars(Long companyId) {
        EntityGraph<Company> graph = entityManager.createEntityGraph(Company.class);
        graph.addAttributeNodes("cars");

        Map<String, Object> hints = new HashMap<>();
        hints.put("javax.persistence.loadgraph", graph);

        return entityManager.find(Company.class, companyId, hints);
    }
}
